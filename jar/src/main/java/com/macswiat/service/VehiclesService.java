package com.macswiat.service;

import com.macswiat.dao.OwnersDao;
import com.macswiat.dao.VehiclesDao;
import com.macswiat.model.Vehicle;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;

@Stateless
public class VehiclesService {

    @EJB
    private VehiclesDao vehiclesDao;

    @EJB
    private OwnersDao ownersDao;

    public Vehicle createVehicle(String vin, String registrationNumber, String owner){
        Vehicle vehicle = new Vehicle();
        vehicle.setVin(vin);
        vehicle.setRegistrationNumber(registrationNumber);
        vehicle.setOwner(ownersDao.findByName(owner));
        vehiclesDao.save(vehicle);
        vehiclesDao.flush();
        return vehicle;
    }


    public JsonArray getAllVehiclesAsJsonArray(boolean addOwner){
        JsonArrayBuilder builder = Json.createArrayBuilder();
        if(addOwner) {
            vehiclesDao.getAll().forEach(v -> builder.add(vehiclesDao.getVehicleWithOwnersJson(v)));
        } else {
            vehiclesDao.getAll().forEach(v -> builder.add(vehiclesDao.getVehicleAsJson(v)));
        }
        return builder.build();
    }
	
	public String getVehicleOwnerName(int vehicleId){
		return "Maciek tortoise merge";
	}
}
