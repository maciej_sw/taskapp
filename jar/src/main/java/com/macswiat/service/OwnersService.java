package com.macswiat.service;

import com.macswiat.dao.OwnersDao;
import com.macswiat.dao.VehiclesDao;
import com.macswiat.model.Owner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;

@Stateless
public class OwnersService {

    public static final Logger logger = LoggerFactory.getLogger(OwnersService.class);

    @EJB
    private OwnersDao ownersDao;

    @EJB
    private VehiclesDao vehiclesDao;

    public boolean ownerExists(String name){
        return ownersDao.ownerExists(name);
    }

    public Owner createOwner(String name){
        Owner owner = new Owner();
        owner.setName(name);
        ownersDao.save(owner);
        ownersDao.flush();
        logger.info("Created owner with id    " + owner.getId());
        return owner;
    }

    public JsonArray getAllOwnersAsJsonArray(){
        JsonArrayBuilder builder = Json.createArrayBuilder();
        ownersDao.getAll().forEach(o -> builder.add(ownersDao.getOwnerAsJson(o)));
        return builder.build();
    }

    public JsonArray getOwnerVehiclesAsJsonArray(String ownerName){
        JsonArrayBuilder builder = Json.createArrayBuilder();
        vehiclesDao.getByOwner(ownerName).forEach(v -> builder.add(vehiclesDao.getVehicleAsJson(v)));
        return builder.build();
    }
	

    public String conflictMethod(){
        return "Conflict commit master";
    }
	
	public String getOwnerName(){
		return "Master tree";
	}
}
