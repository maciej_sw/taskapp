package com.macswiat.dao;

import com.macswiat.model.Vehicle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObject;
import java.util.List;

@Stateless
public class VehiclesDao extends GenericDao<Vehicle> {

    public static final Logger logger = LoggerFactory.getLogger(VehiclesDao.class);

    public List<Vehicle> getAll(){
        return entityManager.createQuery("select v from Vehicle v join v.owner", Vehicle.class).getResultList();
    }

    public List<Vehicle> getByOwner(String owner){
        return entityManager.createQuery("select v from Vehicle v join v.owner o where o.name = :owner_name", Vehicle.class).setParameter("owner_name", owner).getResultList();
    }

    public JsonObject getVehicleAsJson(Vehicle v){
        return Json.createObjectBuilder().add("id", v.getId()).add("vin", v.getVin()).add("registrationNumber", v.getRegistrationNumber()).build();
    }

    public JsonObject getVehicleWithOwnersJson(Vehicle v){
        return Json.createObjectBuilder().add("id", v.getId()).add("vin", v.getVin()).add("registrationNumber", v.getRegistrationNumber()).add("owner", v.getOwner().getName()).build();
    }
}
