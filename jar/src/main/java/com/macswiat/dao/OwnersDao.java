package com.macswiat.dao;

import com.macswiat.model.Owner;
import com.macswiat.model.Vehicle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import java.util.List;

@Stateless
public class OwnersDao extends GenericDao<Owner> {

    public static final Logger logger = LoggerFactory.getLogger(OwnersDao.class);

    public boolean ownerExists(String name){
        try {
            return entityManager.createQuery("select 1 from Owner o where o.name = :name").setParameter("name", name).getSingleResult() != null;
        } catch (NoResultException | NonUniqueResultException ex){
            return false;
        }
    }

    public List<Owner> getAll(){
        return entityManager.createQuery("select o from Owner o", Owner.class).getResultList();
    }

    public Owner findByName(String name){
        try {
            return entityManager.createQuery("select o from Owner o where o.name = :name", Owner.class).setParameter("name", name).getSingleResult();
        } catch (NoResultException ex){
            return null;
        }
    }

    public JsonObject getOwnerAsJson(Owner o){
        return Json.createObjectBuilder().add("id", o.getId()).add("name", o.getName()).build();
    }
}
