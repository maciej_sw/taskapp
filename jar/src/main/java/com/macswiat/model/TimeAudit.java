package com.macswiat.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class TimeAudit {

    private Timestamp created;
    private Timestamp updated;

    @Column(name = "created", nullable = false, updatable = false)
    @NotNull
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Column(name = "updated", insertable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @PrePersist
    void onCreate(){
        this.setCreated(Timestamp.valueOf(LocalDateTime.now()));
    }

    @PreUpdate
    void onUpdate(){
        this.setUpdated(Timestamp.valueOf(LocalDateTime.now()));
    }
}
